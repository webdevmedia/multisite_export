#!/bin/bash

RED='\033[0;31m'
GREEN='\033[3;32;40m'
YELLOW='\033[1;33;40m'
BLUE='\033[3;34;40m'
NC='\033[0m' # No Color
tick="${GREEN}✔${NC}"

dbUser=''
dbPass=''
dbHost=''
dbName=''
blogID=''
tablePrefix=''
domainReplace=''
importDB=''

while [ $# -gt 0 ]; do

   if [[ $1 == *"--"* ]]; then
	    v="${1/--/}"
	    declare $v="$2"
   fi

   shift
done

if ! test -n "$dbName"; then
  printf "${RED}error:${NC} --dbName is undefinde\n" >&2; exit 1
fi

if ! test -n "$dbPass"; then
  printf "${RED}error:${NC} --dbPass is undefinde\n" >&2; exit 1
fi

if ! test -n "$dbUser"; then
  printf "${RED}error:${NC} --dbUser is undefinde\n" >&2; exit 1
fi

if ! test -n "$dbHost"; then
  printf "${RED}error:${NC} --dbHost is undefinde\n" >&2; exit 1
fi

if ! test -n "$tablePrefix"; then
  printf "${RED}error:${NC} --tablePrefix is undefinde\n" >&2; exit 1
fi


if ! test -n "$blogID"; then
	printf "${RED}error:${NC} --blogID is undefinde\n" >&2; exit 1

	if ! [[ $blogID =~ '^[0-9]+$' ]] ; then
   		printf "${RED}error:${NC} blogID: ${blogID} is not a number\n" >&2; exit 1
	fi
fi


if ! test -n "$importDB"; then
importDB="${importDB}"
fi

mysqldump="mysqldump -t -u ${dbUser} -p${dbPass} -h ${dbHost} ${dbName}"

printf "${YELLOW}→${NC} dbHost: ${BLUE}${dbHost}${NC}\n"
printf "${YELLOW}→${NC} dbUser: ${BLUE}${dbUser}${NC}\n"
printf "${YELLOW}→${NC} dbPass: ${BLUE}${dbPass}${NC}\n"
printf "${YELLOW}→${NC} dbName: ${BLUE}${dbName}${NC}\n"
printf "${YELLOW}→${NC} mysqldump: ${BLUE}${mysqldump}${NC}\n\n"


# * Create export folder structur like export/123/2 ---
# * write .version in export/123 so we can do multible exports with same blog id prevent loosing exportfiles
exportBaseDir="export"
exportDir="${exportBaseDir}/${blogID}"

if [ ! -d "${exportDir}" ]; then
   mkdir -p "${exportDir}"
   exportDir="${exportDir}"
fi

if [ ! -f "${exportDir}/.version" ]; then
	echo 1 > "${exportDir}/.version"
else
	echo $( expr $(cat $exportDir/.version) + 1) > "${exportDir}/.version"
fi

version=$( expr $(cat $exportDir/.version) + 1)

if [ ! -d "${exportDir}/${version}" ]; then
   mkdir -p "${exportDir}/${version}"
   exportDir="${exportDir}/${version}"
fi

printf "${tick} export dir ${YELLOW}${exportDir}${NC} erstellt\n"

# * Grep siteID and domain for given blogID
mysql -h $dbHost -u $dbUser -p$dbPass $dbName -Bse "SELECT site_id FROM ${tablePrefix}_blogs WHERE blog_id=${blogID}" > "${exportDir}/.site_id"
mysql -h $dbHost -u $dbUser -p$dbPass $dbName -Bse "SELECT domain FROM ${tablePrefix}_blogs WHERE blog_id=${blogID}" > "${exportDir}/.domain"

printf "${tick} site_id ${BLUE}$(cat ${exportDir}/.site_id)${NC} in ${YELLOW}${exportDir}/.site_id${NC} gespeichert\n"
printf "${tick} domain ${BLUE}$(cat ${exportDir}/.domain)${NC} in ${YELLOW}${exportDir}/.domain${NC} gespeichert\n"

echo "#!/bin/bash" >> ${exportDir}/import.sh

# * Grep data from blogs table and replace domain
mysqldump -u $dbUser -p$dbPass -h $dbHost $dbName -t ${tablePrefix}_blogs --where "blog_id=${blogID}" > ${exportDir}/${tablePrefix}_blogs.sql
echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_blogs.sql" >> ${exportDir}/import.sh
printf "${tick} tabellen aus ${BLUE}${tablePrefix}_blogs${NC} in ${YELLOW}${exportDir}/${tablePrefix}_blogs.sql${NC} exportiert\n"

mysqldump -u $dbUser -p$dbPass -h $dbHost $dbName -t ${tablePrefix}_sitemeta --where "site_id=$(cat ${exportDir}/.site_id)" > ${exportDir}/${tablePrefix}_sitemeta.sql
sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_sitemeta.sql
echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_sitemeta.sql" >> ${exportDir}/import.sh
printf "${tick} side_id ${BLUE}$(cat ${exportDir}/.site_id)${NC} aus ${BLUE}${tablePrefix}_sitemeta${NC} in ${YELLOW}${exportDir}/${tablePrefix}_sitemeta.sql${NC} exportiert\n"

# + Lanuage relation
mysqldump -u $dbUser -p$dbPass -h $dbHost $dbName -t ${tablePrefix}_mlp_site_relations --where "site_1=${blogID} or site_2=${blogID}" > ${exportDir}/${tablePrefix}_mlp_site_relations.sql
sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_mlp_site_relations.sql
echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_mlp_site_relations.sql" >> ${exportDir}/import.sh

mysqldump -u $dbUser -p$dbPass -h $dbHost $dbName -t ${tablePrefix}_multilingual_linked --where "ml_source_blogid=${blogID} or ml_blogid=${blogID}" > ${exportDir}/${tablePrefix}_multilingual_linked.sql
sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_multilingual_linked.sql
echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_multilingual_linked.sql" >> ${exportDir}/import.sh
printf "${tick} Sprachrelationen zu blogID ${BLUE}${blogID}${NC} in ${YELLOW}${exportDir}/${tablePrefix}_mlp_site_relations.sql${NC} und ${YELLOW}${exportDir}/${tablePrefix}_multilingual_linked.sql${NC} exportiert\n"


# * ersetzt domain in _blogs.sql
if test -n "$domainReplace"; then
	domain=$(cat ${exportDir}/.domain)
	sed -i "s/${domain}/${domainReplace}/g" ${exportDir}/${tablePrefix}_blogs.sql
	echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_blogs.sql" >> ${exportDir}/import.sh
	printf "${tick} domain ${BLUE}${domain}${NC} mit ${BLUE}${domainReplace}${NC} in ${YELLOW}${exportDir}/${tablePrefix}_blogs.sql${NC} ersetzt\n"
fi


mysql -u $dbUser -p$dbPass -h $dbHost -N information_schema -e "select table_name from tables where table_schema = '${dbName}' and table_name like '%${tablePrefix}\_${blogID}\_%'" > "${exportDir}/.tables"
mysqldump -u $dbUser -p$dbPass -h $dbHost  $dbName `cat ${exportDir}/.tables` >> ${exportDir}/${tablePrefix}_${blogID}.sql
echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_${blogID}.sql" >> ${exportDir}/import.sh
printf "${tick} tabellen zu blogID ${BLUE}${blogID}${NC} in ${YELLOW}${exportDir}/${tablePrefix}_${blogID}.sql${NC} exportiert\n"

if [ -f ${exportBaseDir}/.known_userIDs ]; then
   	mv ${exportBaseDir}/.known_userIDs ${exportBaseDir}/.known_userIDs.tmp
	printf "${tick} .known_userIDs.tmp in ${YELLOW}export/${NC} erstellt\n"
else
	mysql -u $dbUser -p$dbPass -h $dbHost $dbName -Bse "select distinct user_id from ${tablePrefix}_usermeta where meta_key like '%${tablePrefix}\_${blogID}\_%'" > ${exportBaseDir}/.known_userIDs.tmp
	printf "${tick} .known_userIDs.tmp in neu ${YELLOW}export/${NC} erstellt\n"
fi


mysql -u $dbUser -p$dbPass -h $dbHost $dbName -Bse "select distinct user_id from ${tablePrefix}_usermeta where meta_key like '%${tablePrefix}\_${blogID}\_%'" > ${exportBaseDir}/.known_userIDs

printf "${tick} user zu blogID ${BLUE}${blogID}${NC} in ${YELLOW}export/.known_userIDs${NC} geschreiben\n"

userids=$(cat ${exportBaseDir}/.known_userIDs)

if [ -f ${exportBaseDir}/.known_userIDs.tmp ]; then
	knownUserIDs=$(cat ${exportBaseDir}/.known_userIDs.tmp)

	IFS=$'\n' set -f
	for i in $userids; do
		if [ -z $(cat ${exportBaseDir}/.known_userIDs.tmp | grep "^$i$") ]; then
			${mysqldump} ${tablePrefix}_users --where "ID=${i}"  >> ${exportDir}/${tablePrefix}_${blogID}_${i}_users.sql
			${mysqldump} ${tablePrefix}_usermeta --where "user_id=${i} and meta_key not like '%${tablePrefix}_%'"  >> ${exportDir}/${tablePrefix}_${blogID}_${i}_usermeta.sql
			sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_${blogID}_${i}_users.sql
			sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_${blogID}_${i}_usermeta.sql
			echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_${blogID}_${i}_usermeta.sql" >> ${exportDir}/import.sh
			echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_${blogID}_${i}_users.sql" >> ${exportDir}/import.sh
			printf "${tick} neue user zu blogID ${BLUE}${blogID}${NC} nach ${YELLOW}${exportDir}/${tablePrefix}_${blogID}_${i}_usermeta.sql${NC} exportiert\n"
		fi

		${mysqldump} ${tablePrefix}_usermeta --where "user_id=${i} and meta_key like '%${tablePrefix}_${blogID}%'"  >> ${exportDir}/${tablePrefix}_${blogID}_usermeta.sql
		sed -i "s/([0-9]*,/(NULL,/gi" ${exportDir}/${tablePrefix}_${blogID}_usermeta.sql
		printf "${tick} usermeta aus blogID ${BLUE}${blogID}${NC} zu userId ${BLUE}${i}${NC} nach ${YELLOW}${exportDir}/${tablePrefix}_${blogID}_usermeta.sql${NC} exportiert\n"
	done

   rm ${exportBaseDir}/.known_userIDs.tmp
   printf "${tick} known_userIDs gesaeubernt\n"
   echo "mysql -f -u ${dbUser} -p${dbPass} ${importDB} < ${exportDir}/${tablePrefix}_${blogID}_usermeta.sql" >> ${exportDir}/import.sh
fi

chmod -x ${exportDir}/import.sh

#domain="http:\/\/${domain}"

#php sr.cli.php -s $(${domain} | sed -i "s/www//g") -r @${domainReplace} -f ${exportDir}/${tablePrefix}_${blogID}.sql
#php sr.cli.php -s ${domain} -r ${domainReplace} -f ${exportDir}/${tablePrefix}_${blogID}.sql

#sed -i "s|${domain}|${domainReplace}|g" ${exportDir}/${tablePrefix}_${blogID}.sql

#printf "${tick} suche nach ${BLUE}${domain}${NC} ersetzte durch ${BLUE}${domainReplace}${NC} in ${YELLOW}${exportDir}/${tablePrefix}_${blogID}.sql${NC} \n"
#printf "${YELLOW}→${NC} mehr Informationen in ${BLUE}${exportDir}/replace.log${NC}\n"

