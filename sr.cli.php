#!/usr/bin/php -q

<?php

$arg_count 	= $_SERVER[ 'argc' ];
$args_array = $_SERVER[ 'argv' ];

$opts = [
			's:' => 'search:',
			'r:' => 'replace:',
			'f:' => 'file:',
		];

$required = [ 's:', 'r:', 'f:'];

$short_opts = array_keys( $opts );
$short_opts_normal = array_map( 'strip_colons', $short_opts );

$long_opts = array_values( $opts );
$long_opts_normal = array_map( 'strip_colons', $long_opts );

$options = getopt( implode( '', $short_opts ), $long_opts );

$missing_arg = '';

// check required args are passed
foreach( $required as $key ) {
	$short_opt = strip_colons( $key );
	$long_opt = strip_colons( $opts[ $key ] );
	if ( ! isset( $options[ $short_opt ] ) && ! isset( $options[ $long_opt ] ) ) {
		fwrite( STDERR, "Error: Missing argument, -{$short_opt} or --{$long_opt} is required.\n" );
		$missing_arg = true;
	}
}

// bail if requirements not met
if ( $missing_arg ) {
	fwrite( STDERR, "Please enter the missing arguments.\n" );
	exit( 1 );
}

function strip_colons( $string ) {
	return str_replace( ':', '', $string );
}

$logName = dirname($options['f']) . '/replace.log';
$pattern = 'a:\d+:{.*?' . $options['s'] . '.*?}';
$content = file_get_contents($options['f']);

preg_match_all('/' . $pattern . '/', $content, $match);

if(!empty($match[0])) {
	foreach ($match[0] as $serialized_array) {

		$a = explode('s:', $serialized_array);

		foreach($a as $i => $s){
			if(!empty(strpos($s, str_replace( '\/', '/', $options['s'])))){

				preg_match('/^(\d+):\\\"(.*?)\\\";$/', $s, $str);

				if(!empty($str)){

					$str[2] = str_replace(['\n','\r'], ['~','µ'], $str[2]);
					$str[4] = str_replace(str_replace( '\/', '/', $options['s']), $options['r'], $str[2]);
					$str[5] = mb_strlen($str[4], 'utf8');
					$str[6] = str_replace(['~','µ'], ['\n','\r'], $str[4]);

					$str[7] = 's:' . $str[5] . ':\"' . $str[6] . '\";';

					$content = str_replace($str[0], $str[7], $content);
					$replacement = "search: " . $str[ 0 ] . "\nreplace: " . $str[7] . "\n\n";

					file_put_contents( $logName, $replacement, FILE_APPEND );
				}

			}
		}

	}



	file_put_contents( $options['f'], $content);
}

